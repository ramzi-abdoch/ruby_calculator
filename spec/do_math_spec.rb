#require "do_math"
#Mather = DoMath.new

describe "Addition" do
  context "given x = 4 and y = 2" do
    it "returns six" do
      expect(Mather.+(4,2)).to eql(6)
    end
  end
end
